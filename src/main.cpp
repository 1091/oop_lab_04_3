#include "cpassword.h"
#include "cloginpage.h"
#include "cauthtoken.h"
#include "ipage.h"
#include <string>
#include <termios.h>
#include <unistd.h>
#include <iostream>

int main(int argc, char **argv){
	cLoginPage loginPage;
	loginPage.show();
	cAuthToken token = loginPage.getToken();
	return 0;
}

