#ifndef CAUTHTOKEN_H
#define CAUTHTOKEN_H

#include <string>
#include "cpassword.h"

class cAuthToken {
public:
	cAuthToken(std::string login, cPassword password) : login(login), password(password) {}
private:
	std::string login;
	cPassword password;
};

#endif // CAUTHTOKEN_H
