#ifndef CPOST_H
#define CPOST_H

#include <string>
#include "cdate.h"
#include "icommentable.h"

class cPost : public iCommentable{
public:
	cPost(std::string& header, std::string& text);
	std::string getText();
	std::string getHeader();
	cDate getDate();
	void setText(std::string& text);
	void setHeader(std::string& header);
	void disableCommenting();
	void enableCommenting();
	void addComment();
	bool deleteComment(int number);
private:
	std::string header;
	cDate date;
	std::string text;
	bool commentingIsEnable;
};

#endif // CPOST_H
