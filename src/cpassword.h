#ifndef CPASSWORD_H
#define CPASSWORD_H

#include <string>
#include <iostream>

class cPassword {
public:
	cPassword(const std::string& password);
	bool isHashEqualTo(size_t hash);
private:
	size_t hash;
	static constexpr int minimalLength = 8;
};

#endif // CPASSWORD_H
