#ifndef CCOMMENT_H
#define CCOMMENT_H

#include "icommentable.h"
#include <vector>

class cComment : public iCommentable{
public:
	cComment();
private:
	std::vector<cComment> comments;
};

#endif // CCOMMENT_H
