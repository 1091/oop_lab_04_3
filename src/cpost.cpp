#include "cpost.h"

cPost::cPost(std::string &header, std::string &text){
	this->header = header;
	this->text = text;
	this->date = cDate();
	commentingIsEnable = true;
}

std::string cPost::getHeader(){
	return header;
}

std::string cPost::getText(){
	return text;
}

cDate cPost::getDate(){
	return date;
}

void cPost::setHeader(std::string& header){
	this->header = header;
}

void cPost::setText(std::string &text){
	this->text = text;
}

void cPost::disableCommenting(){
	commentingIsEnable = false;
}

void cPost::enableCommenting(){
	commentingIsEnable = true;
}

void cPost::addComment(){

}

bool cPost::deleteComment(int number){

}
