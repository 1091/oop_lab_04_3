#ifndef CPASSWORDINPUT_H
#define CPASSWORDINPUT_H

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <string>
#include <iostream>

class cPasswordInput {
protected:
	std::string getPassword();
};

#endif // CPASSWORDINPUT_H
