#ifndef CCOMMENTABLE_H
#define CCOMMENTABLE_H

class iCommentable {
public:
	virtual void addComment() = 0;
	virtual bool deleteComment(int number) = 0;
};

#endif // CCOMMENTABLE_H
