#ifndef CBLOG_H
#define CBLOG_H

#include <string>
#include <vector>
#include "chomepage.h"

class cBlog{
public:
	cBlog();
private:
	std::string name;
	std::vector<cHomePage> pages;
};

#endif // CBLOG_H
