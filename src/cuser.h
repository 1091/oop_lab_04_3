#ifndef CUSER_H
#define CUSER_H

#include <string>
#include "cblog.h"
#include "cpassword.h"
#include "cdate.h"

class cUser{
public:
	cUser();
	bool deleteBlog();
	void setName(const std::string& name);
	void setInterests(const std::string& interests);
	void setContacts(const std::string& contacts);
	void setInfo(const std::string& info);
	void setPassword(cPassword* password);
	void setBirthday(cDate& birthday);
private:
	cBlog *blog;
	std::string name;
	std::string interests;
	cDate birthday;
	std::string contacts;
	std::string info;
	cPassword* password;
};

#endif // CUSER_H
