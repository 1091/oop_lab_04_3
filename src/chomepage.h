#ifndef CHOMEPAGE_H
#define CHOMEPAGE_H

#include "ipage.h"
#include "cpost.h"
#include <vector>
#include <iostream>

class cHomePage : public iPage {
public:
	cHomePage();
	~cHomePage();
	void show();
private:
	int currentNumber;
	std::vector<cPost> posts;
	static constexpr int capacity = 10;
};

#endif // CHOMEPAGE_H
