#include "cpasswordinput.h"

std::string cPasswordInput::getPassword(){
	termios oldt;
	tcgetattr(STDIN_FILENO, &oldt);
	termios newt = oldt;
	newt.c_lflag &= ~ECHO;
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	std::string password;
	std::getline(std::cin, password);
	return password;
}
