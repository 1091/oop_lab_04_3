TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    cuser.cpp \
    cblog.cpp \
    cpassword.cpp \
    cdate.cpp \
    chomepage.cpp \
    cpost.cpp \
    ccomment.cpp \
    cloginpage.cpp \
    cpasswordinput.cpp \
    csession.cpp

HEADERS += \
    cuser.h \
    cblog.h \
    cpassword.h \
    cdate.h \
    chomepage.h \
    cpost.h \
    ccomment.h \
    icommentable.h \
    ipage.h \
    cloginpage.h \
    cpasswordinput.h \
    cauthtoken.h \
    csession.h

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -I/usr/include/mysql
QMAKE_LIBS += /usr/lib/libmysqlpp.so
