#ifndef CLOGINPAGE_H
#define CLOGINPAGE_H

#include "cpassword.h"
#include "cpasswordinput.h"
#include "cauthtoken.h"
#include "ipage.h"
#include <iostream>
#include <string>

class cLoginPage : public iPage, private cPasswordInput {
public:
	cLoginPage();
	void show();
	virtual cAuthToken getToken();
private:
	std::string login;
	cPassword password;
};

#endif // CLOGINPAGE_H
