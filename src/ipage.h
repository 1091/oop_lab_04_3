#ifndef CPAGE_H
#define CPAGE_H

class iPage {
public:
	virtual void show() = 0;
	virtual ~iPage() {}
};

#endif // CPAGE_H
